import { Component, OnInit } from '@angular/core';
import { NavigationService } from 'src/app/services/navigation.service';
import { Router } from '@angular/router';
import { DinosaureData } from '../inscription/inscription.component';
import { CrudService } from 'src/app/services/crud.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  hide = true;
  dinosaure = new DinosaureData();
  erreur = false;
  constructor(private navigationService: NavigationService, private _router: Router, private crudService: CrudService) { }

  ngOnInit() {
    this.navigationService.login = true;
    this.navigationService.inscription = false;
  }

  Connexion() {

    this.crudService.connexion(this.dinosaure).subscribe(response => {
      this.navigationService.login = false;
      this.navigationService.inscription = false;
      this.navigationService.connected = true;
      sessionStorage.setItem('dinosaure', JSON.stringify(response));
      this._router.navigate(['accueil']);

    }, error => {
      this.erreur = true;
    })


  }

  close() {
    this.erreur = false;
  }
  closeCreation() {
    this.navigationService.creation = false;
  }
}
