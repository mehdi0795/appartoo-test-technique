import { Component, OnInit } from '@angular/core';
import { NavigationService } from 'src/app/services/navigation.service';
import { CrudService } from 'src/app/services/crud.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-inscription',
  templateUrl: './inscription.component.html',
  styleUrls: ['./inscription.component.css']
})



export class InscriptionComponent implements OnInit {
  firstPart = true;
  secondPart = false;
  hide = true;
  erreur = false;
  dinosaure = new DinosaureData();
  constructor(private navigationService: NavigationService, private crudService: CrudService, private _router: Router) { }

  ngOnInit() {

  }

  previousPart() {
    this.secondPart = false;
    this.firstPart = true;

  }

  nextPart() {
    this.firstPart = false;
    this.secondPart = true;


  }

  Valider() {
    console.log(this.dinosaure);
    this.crudService.createDinosaure(this.dinosaure).subscribe(response => {
      console.log(response);
      this.navigationService.creation = true;
      this._router.navigate(['login']);


    }, error => {
      this.erreur = true
    }
    )
  }

  close() {
    this.erreur = false;
  }

}

export class DinosaureData {
  famille: string;
  race: string;
  nourriture: string;
  age: number;
  email: string;
  password: string;
}
