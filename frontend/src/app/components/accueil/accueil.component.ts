import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { CrudService } from 'src/app/services/crud.service';
import { HttpHeaders } from '@angular/common/http';
import { DinosaureData } from '../inscription/inscription.component';


@Component({
  selector: 'app-accueil',
  templateUrl: './accueil.component.html',
  styleUrls: ['./accueil.component.css']
})

export class AccueilComponent implements OnInit {
  ajoutNouvelAmi=false
  friendDeleted = false;
  friendAdded = false;
  firstPart = true;
  secondPart = false;
  hide = true;
  erreur = false;
  displayedColumns: string[] = ['famille', 'race', 'nourriture', 'age', 'supprimer'];
  displayedColumns2: string[] = ['famille', 'race', 'nourriture', 'age', 'ajouter','ajouterNouvelAmi'];
  dataSource: MatTableDataSource<Object>;
  dataSource2: MatTableDataSource<Object>;
  dinosaure = new DinosaureData();
  dinosaure2 = new DinosaureData();
  allFriends = true;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private crudService: CrudService) {



  }

  ngOnInit() {

    this.dinosaure = JSON.parse(sessionStorage.getItem('dinosaure'))['foundDinosaure'];

    this.refresh();
  }
  supprimer(idFriend) {


    this.crudService.deleteFriend(this.dinosaure['_id'], idFriend).subscribe(response => {
      this.friendDeleted = true;
      this.refresh();

    })

  }

  previousPart() {
    this.secondPart = false;
    this.firstPart = true;

  }

  nextPart() {
    this.firstPart = false;
    this.secondPart = true;


  }
  refresh() {
    this.crudService.getNotFriends(this.dinosaure).subscribe(response => {
      this.dataSource2 = new MatTableDataSource(response);
      this.dataSource2.paginator = this.paginator;
      this.dataSource2.sort = this.sort;



    })

    this.crudService.getFriends(this.dinosaure).subscribe(response => {
      console.log('friends');
      console.log(response);
      this.dataSource = new MatTableDataSource(response);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;

    })
  }

  close() {
    this.friendDeleted = false;
  }
  close2() {
    this.friendAdded = false;
  }
  pageAjout() {
    this.allFriends = false;
  }
  retour() {
    this.allFriends = true;
  }

  ajouterAmi(idFriend) {
    console.log(idFriend);
    console.log(this.dinosaure['_id']);
    this.crudService.addFriend(this.dinosaure['_id'], idFriend).subscribe(response => {
      this.friendAdded = true;
      this.refresh();

    })

  }

  ajouterNouvelAmi(){
      this.ajoutNouvelAmi=true;
      
  }

  Valider(){
    this.crudService.createDinosaure(this.dinosaure2).subscribe(response => {
      console.log('am heeeeeeeeeeeere');
      console.log(response);
      console.log(response['_id']);
      console.log(this.dinosaure['_id']);
      this.crudService.addFriend(this.dinosaure['_id'],response['_id']).subscribe(response=>{
        console.log('done');
        this.ajoutNouvelAmi=false;
        this.allFriends=true
        this.refresh();

      })
    })
      
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

}
