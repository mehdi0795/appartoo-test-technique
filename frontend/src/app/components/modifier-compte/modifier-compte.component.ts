import { Component, OnInit } from '@angular/core';
import { DinosaureData } from '../inscription/inscription.component';
import { CrudService } from 'src/app/services/crud.service';
import { Router } from '@angular/router';
import { NavigationService } from 'src/app/services/navigation.service';

@Component({
  selector: 'app-modifier-compte',
  templateUrl: './modifier-compte.component.html',
  styleUrls: ['./modifier-compte.component.css']
})
export class ModifierCompteComponent implements OnInit {

  dinosaure = new DinosaureData();
  modification = false;
  erreur = false;
  constructor(private crudService: CrudService, private _router: Router, private navigationService: NavigationService) { }

  ngOnInit() {

    this.dinosaure = JSON.parse(sessionStorage.getItem('dinosaure'))['foundDinosaure'];

    this.crudService.getUserById(this.dinosaure['_id']).subscribe(response => {
      this.dinosaure = response;
    })

  }

  modifier() {
    this.crudService.updateDinosaure(this.dinosaure['_id'], this.dinosaure).subscribe(response => {
      console.log(this.dinosaure);
      this.modification = true;

    }, error => {
      console.log('erreur');
      this.erreur = true;
    }
    )

  }

  close() {
    this.modification = false;
    this.navigationService.accueil = true;
    this._router.navigate(['accueil']);

  }
}
