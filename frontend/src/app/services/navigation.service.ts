import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class NavigationService {
  inscription=false;
  login=true;
  connected=false;
  accueil=true;
  creation=false;
  constructor() { }
}
