import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpHeaders } from '@angular/common/http';
import { DinosaureData } from '../components/inscription/inscription.component';

@Injectable({
  providedIn: 'root'
})
export class CrudService {

  constructor(private http: HttpClient) { }

  createDinosaure(dinosaureData) {

    return this.http.post(`http://localhost:3000/dinosaures/register`, dinosaureData);

  }

  connexion(dinosaureData) {

    return this.http.post(`http://localhost:3000/dinosaures/authenticate`, dinosaureData);
  }

  getNotFriends(connectedUser) {

    console.log(connectedUser);
    return this.http.get<Object[]>(`http://localhost:3000/dinosaures/${connectedUser._id}`, this.jwt());
  }

  getFriends(connectedUser) {
    return this.http.get<Object[]>(`http://localhost:3000/dinosaures/getFriends/${connectedUser._id}`, this.jwt());
  }

  updateDinosaure(id, dinosaureData) {
    return this.http.put(`http://localhost:3000/dinosaures/${id}`, dinosaureData, this.jwt());
  }

  getUserById(id) {
    return this.http.get<DinosaureData>(`http://localhost:3000/dinosaures/getById/${id}`, this.jwt());
  }

  addFriend(id, idFriend) {
    return this.http.put(`http://localhost:3000/dinosaures/addFriend/${id}/${idFriend}`, {}, this.jwt());
  }

  deleteFriend(id, idFriend) {
    return this.http.put(`http://localhost:3000/dinosaures/deleteFriend/${id}/${idFriend}`, {}, this.jwt());
  }

  jwt() {
    // create authorization header with jwt token
    let currentUser = JSON.parse(sessionStorage.getItem('dinosaure'));
    if (currentUser && currentUser.token) {
      let headers: HttpHeaders = new HttpHeaders();
      headers = headers.append('Authorization', 'Bearer ' + currentUser.token);
      return { headers };
    }
  }
}
