import { Component } from '@angular/core';
import { NavigationService } from './services/navigation.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'TestAppartooFront';

  constructor(private navigationService : NavigationService,private _router: Router){}

  ngOnInit() {
    
    
  }
  PageInscription(){
    this.navigationService.login=false;
    this.navigationService.inscription=true;
    this._router.navigate(["inscription"]);
  }

  PageLogin(){
    this.navigationService.login=true;
    this.navigationService.inscription=false;
    this._router.navigate(["login"]);
  }

  Deconnexion(){
    this.navigationService.login=true;
    this.navigationService.inscription=false;
    this.navigationService.connected=false;
    this.navigationService.accueil=true;
    sessionStorage.clear();
    this._router.navigate(["login"]);
  }
  pageModification(){
    this.navigationService.accueil=false;
    this._router.navigate(["modifierCompte"]);
  }

  pageAccueil(){
    this.navigationService.accueil=true;
    this._router.navigate(["accueil"]);
  }
  
}
