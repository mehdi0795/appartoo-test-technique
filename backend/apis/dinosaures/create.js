
const createError = require("http-errors");
var bcrypt = require('bcrypt');
const Dinosaure = require("../../models/Dinosaure");
const _ = require("lodash");

const createDinosaureHandler = async dinosaureData => {
    if (_.isEmpty(dinosaureData)) {
        throw createError(400, "No data posted. Please fill some data !");
    }

    const {email} = (dinosaureData || {});

    const existingDinosaure = await Dinosaure.findOne({email});
    if (existingDinosaure) {
        throw createError(409, "This email already exists for another Dinosaure");
    }

    const newDinosaure = new Dinosaure({
        ...dinosaureData,
        password: bcrypt.hashSync(dinosaureData.password
            , bcrypt.genSaltSync())
    });

    const savedDinosaure = await newDinosaure.save();
    return savedDinosaure;
};

module.exports = createDinosaureHandler;
