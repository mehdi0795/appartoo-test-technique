const createError = require("http-errors");
const Dinosaure = require("../../models/Dinosaure");

/*
 * Update an existing Dinosaure.
 */
const updateHandler = async (id, newDinosaureData) => {
    const DinosaureToUpdate = await Dinosaure.findById(id);
    if (!DinosaureToUpdate) {
        throw createError(404, `Dinosaure not found for #ID: ${id}.`);
    }

    // Data not allowed to be updated.
    delete newDinosaureData.__v;
    delete newDinosaureData._id;

    Object.keys(newDinosaureData).forEach(path => DinosaureToUpdate.set(path, newDinosaureData[path]));

    const updatedDinosaure = await DinosaureToUpdate.save();
    return updatedDinosaure;
};

const addFriendHandler = async (connectedDinosaureId, friendId) => {
    const dinosaureToUpdate = await Dinosaure.findById(connectedDinosaureId);
    

    if (!dinosaureToUpdate) {
        throw createError(404, `Dinosaure not found for #ID: ${connectedDinosaureId}.`);
    }

    if(friendId) {
        const friendToUpdate = await Dinosaure.findById(friendId);

        if (!friendToUpdate) {
            throw createError(404, `Dinosaure not found for #ID: ${friendId}.`);
        }
        dinosaureToUpdate.amis.push(friendId);
        friendToUpdate.amis.push(connectedDinosaureId);
        await friendToUpdate.save();
    }
    const updatedDinosaure = await dinosaureToUpdate.save();

    return updatedDinosaure;
};

const deleteFriendHandler = async (connectedDinosaureId, friendId) => {

    const dinosaureToUpdate = await Dinosaure.findById(connectedDinosaureId);
    const friendToUpdate = await Dinosaure.findById(friendId);

    if (!dinosaureToUpdate) {
        throw createError(404, `Dinosaure not found for #ID: ${connectedDinosaureId}.`);
    }
    if (!friendToUpdate) {
        throw createError(404, `Dinosaure not found for #ID: ${friendId}.`);
    }

    dinosaureToUpdate.amis.pull({_id: friendId});
    const updatedDinosaure = await dinosaureToUpdate.save();

    friendToUpdate.amis.pull({_id: connectedDinosaureId});
    await friendToUpdate.save();

    return updatedDinosaure;
};

module.exports = {updateHandler, addFriendHandler, deleteFriendHandler};