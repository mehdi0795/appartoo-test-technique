
const createError = require("http-errors");
var bcrypt = require("bcrypt");
const Dinosaure = require("../../models/Dinosaure");
const _ = require("lodash");
const jwt = require("jsonwebtoken");

const authenticateHandler = async dinosaureData => {

    const foundDinosaure = await Dinosaure.findOne({ email: dinosaureData.email });
    if(!foundDinosaure) {
        throw createError(400, "Email not found");
    }

    if (bcrypt.compareSync(dinosaureData.password, foundDinosaure.password)) 
    {
        const token = jwt.sign({ email: dinosaureData.email }
            , "secret"
            , { expiresIn: "24h" });
        const creditentials = {
            foundDinosaure,
            
            token
        };
        return creditentials;
    }
    else {
        throw createError(400,"Wrong password");
    }
};

module.exports = authenticateHandler;
