const createError = require("http-errors");

const Dinosaure = require("../../models/Dinosaure");

const getHandler = async id => {

    const dinosaure = await Dinosaure.findById(id).populate("amis");
    if (!dinosaure) {
        throw createError(404, `Dinosaure not found for #ID: ${id}.`);
    }
    return dinosaure;
};

module.exports = getHandler;
