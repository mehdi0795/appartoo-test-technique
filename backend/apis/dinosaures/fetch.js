const Dinosaure = require("../../models/Dinosaure");

const fetchAllHandler = async connectedUserId => {

    const connectedDinosaure = await Dinosaure.findById(connectedUserId);
    const friends = connectedDinosaure.amis;
    friends.push(connectedUserId);
    const dinosaures = await Dinosaure.find({_id: {$nin: friends}});
    return dinosaures;
};


const getAllFriends = async connectedUserId => {
    const connectedDinosaure = await Dinosaure.findById(connectedUserId);
    const friendsOfConnectedUser = await Dinosaure.find({_id: {$in : connectedDinosaure.amis}});
    return friendsOfConnectedUser;
};

module.exports = {fetchAllHandler,getAllFriends};

