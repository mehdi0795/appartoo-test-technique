const createError = require("http-errors");
const _ = require("lodash");

function processError(err, next) {
    console.log("utils: ", _.keys(err));
    if (err.status) {
        if (err.status === 400) {
            next({status: 400, message: err.message, path: err.path});
        } else {
            next(createError(err.status, err.message, err));
        }
    } else {
        next(createError(500, err.message, err));
    }
}

module.exports = {processError};
