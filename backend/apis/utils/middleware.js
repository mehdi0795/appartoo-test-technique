var jwt = require("jsonwebtoken");
const {processError} = require("../../apis/utils/utils");

module.exports = (req, res, next) => {
    const header = req.headers.authorization;
    try {
        const token = header.split(" ")[1];
        const decodedToken = jwt.verify(token, "secret");
        if (decodedToken) {
            next();
        }
    } catch (err) {
        console.log(err);
        processError(err, next);
    }
};


