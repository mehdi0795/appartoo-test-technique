const mongoose = require("mongoose");
var valid = require("validator");

const dinosaureSchema = new mongoose.Schema({
    email : {
        type : String,
        required : true,
        minlength: 1,
        trim : true,
        validate:{
            validator:valid.isEmail,
            message:"{VALUE} Email is not valid"}
    },   
    password: String,
    age: Number,
    famille: String,
    race: String,
    nourriture: String,
    amis: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: "Dinosaure"
    }]
}, {collection : "dinosaures"});

module.exports = mongoose.model("Dinosaure", dinosaureSchema);