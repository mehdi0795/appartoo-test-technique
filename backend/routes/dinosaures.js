const express = require("express");
const router = express.Router();
const {processError} = require("../apis/utils/utils");
const createDinosaureHandler = require("../apis/dinosaures/create");
const authenticateHandler = require("../apis/dinosaures/authenticate");
const auth = require("../apis/utils/middleware");
const {fetchAllHandler, getAllFriends} = require("../apis/dinosaures/fetch");
const getDinosaureHandler = require("../apis/dinosaures/get");
const {updateHandler, addFriendHandler, deleteFriendHandler} = require("../apis/dinosaures/update");


/**
 * POST: Create a new Dinosaure
 */
router.post("/register", async (req, res, next) => {
    try {
        res.status(200).json(await createDinosaureHandler(req.body));
    } catch (err) {
        processError(err, next);
    }
});

/**
 * POST: Authentication route
 */
router.post("/authenticate", async (req, res, next) => {
    try {
        res.status(200).json(await authenticateHandler(req.body));
    } catch (err) {
        processError(err, next);
    }
});

/* GET: get dinosaures list. */
router.get("/:connectedUser", auth, async (req, res, next) => {
    console.log("here", req.params.connectedUser);
    try {
        res.json(await fetchAllHandler(req.params.connectedUser));
    } catch (err) {
        processError(err, next);
    }
});

/* GET: get dinosaure by its ID. */
router.get("/getById/:id", auth, async (req, res, next) => {
    try {
        res.json(await getDinosaureHandler(req.params.id));
    } catch (err) {
        processError(err, next);
    }
});

/* PUT: UPDATE a Dinosaure. */
router.put("/:id", auth, async (req, res, next) => {
    try {
        res.status(200).json(await updateHandler(req.params.id, req.body));
    } catch (err) {
        processError(err, next);
    }
});

router.get("/getFriends/:id", auth, async (req, res, next) =>{
    try {
        res.status(200).json(await getAllFriends(req.params.id))
    } catch (error) {
        processError(error, next);
    }
})

/* PUT: Add a friend. */
router.put("/addFriend/:id/:friendId", auth, async (req, res, next) => {
    try {
        res.status(200).json(await addFriendHandler(req.params.id, req.params.friendId));
    } catch (err) {
        processError(err, next);
    }
});

router.put("/deleteFriend/:id/:friendId", auth, async (req, res, next) => {
    try {
        res.status(200).json(await deleteFriendHandler(req.params.id, req.params.friendId));

    } catch (err) {
        processError(err, next);
    }
});

module.exports = router;